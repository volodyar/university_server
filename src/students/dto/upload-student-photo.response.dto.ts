import { ApiProperty } from '@nestjs/swagger';

export class UploadStudenPhotoResponseDto {
  @ApiProperty({ default: 'students/64f6248f2acda10180f9edb6.jpg' })
  imagePath: string;
}
