import { DataSource, Repository } from 'typeorm';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import {
  CreateMarkRequestDto,
  CreateMarkResponseDto,
  UpdateMarkRequestDto,
} from './dto';

@Injectable({})
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly marksRepository: Repository<Mark>,
    @InjectDataSource() private readonly dataSourse: DataSource,
  ) {}

  async create(dto: CreateMarkRequestDto): Promise<CreateMarkResponseDto> {
    const lectorCourse = await this.dataSourse
      .createQueryBuilder()
      .select(['lector_course.lector_id', 'lector_course.course_id'])
      .from('lector_course', 'lector_course')
      .where('lector_id = :lectorId', { lectorId: dto.lectorId })
      .andWhere('course_id = :courseId', { courseId: dto.courseId })
      .getRawOne();

    if (!lectorCourse)
      throw new BadRequestException("The course isn't found in lector");

    const studentCourse = await this.dataSourse
      .createQueryBuilder()
      .select(['student_course.student_id', 'student_course.course_id'])
      .from('student_course', 'student_course')
      .where('student_id = :studentId', { studentId: dto.studentId })
      .andWhere('course_id = :courseId', { courseId: dto.courseId })
      .getRawOne();

    if (!studentCourse)
      throw new BadRequestException("The course isn't found in student");

    return await this.marksRepository.save(dto);
  }

  async patch(id: number, dto: UpdateMarkRequestDto): Promise<void> {
    const result = await this.marksRepository.update(id, dto);

    if (!result.affected) throw new NotFoundException('Student not found');
  }

  async delete(id: number): Promise<void> {
    const result = await this.marksRepository.delete(id);

    if (!result.affected) throw new NotFoundException('Student not found');
  }
}
