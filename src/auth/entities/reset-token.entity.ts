import { Column, Entity } from 'typeorm';
import { CommonEntity } from '../../application/entity';

@Entity({ name: 'reset_token' })
export class ResetToken extends CommonEntity {
  @Column({ type: 'int' })
  userId: number;

  @Column({ type: 'varchar' })
  token: string;
}
