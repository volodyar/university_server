import { Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Group } from './entities/group.entity';
import {
  CreateGroupRequestDto,
  CreateGroupResponseDto,
  FindAllGroupsResponseDto,
  UpdateGroupRequestDto,
  UpdateGroupResponseDto,
} from './dto';
import { QueryFilterDto } from '../application/dto';

@Injectable({})
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupsRepository: Repository<Group>,
  ) {}

  async findAll(query: QueryFilterDto): Promise<FindAllGroupsResponseDto> {
    const { limit, offset, order, sort, name } = query;
    const page = (offset - 1) * limit;

    const [groups, count] = await this.groupsRepository
      .createQueryBuilder('groups')
      .leftJoinAndSelect('groups.students', 'students')
      .where('groups.name ilike :name', { name: `%${name ? name : ''}%` })
      .orderBy(`groups.${sort}`, order)
      .take(limit)
      .skip(page)
      .getManyAndCount();

    return { groups, count };
  }

  async findOne(id: number): Promise<CreateGroupResponseDto | object> {
    const group = await this.groupsRepository
      .createQueryBuilder('groups')
      .leftJoinAndSelect('groups.students', 'students')
      .where('groups.id = :id', { id })
      .getOne();

    if (!group) {
      return {};
    }

    return group;
  }

  async create(dto: CreateGroupRequestDto): Promise<CreateGroupResponseDto> {
    return await this.groupsRepository.save(dto);
  }

  async patch(
    id: number,
    dto: UpdateGroupRequestDto,
  ): Promise<UpdateGroupResponseDto> {
    const result = await this.groupsRepository.update(id, dto);

    if (!result.affected) throw new NotFoundException('Group not found');

    return await this.groupsRepository.findOneBy({ id });
  }

  async delete(id: number): Promise<void> {
    const result = await this.groupsRepository.delete(id);

    if (!result.affected) throw new NotFoundException('Group not found');
  }
}
