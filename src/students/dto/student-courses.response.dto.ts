import { ApiProperty } from '@nestjs/swagger';
import { CreateStudentResponseDto } from './create-student.response.dto';
import { CreateCourseResponseDto } from '../../courses/dto';

export class StudentCoursesResponseDto extends CreateStudentResponseDto {
  @ApiProperty({ type: [CreateCourseResponseDto] })
  courses: [CreateCourseResponseDto];
}
