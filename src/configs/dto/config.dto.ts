import { IsNumberString, IsString, IsNotEmpty, IsEnum } from 'class-validator';
import { AppEnvs } from '../../application/enums';

export class ConfigDto {
  @IsNotEmpty()
  @IsString()
  @IsEnum(AppEnvs)
  NODE_ENV: AppEnvs;

  @IsNotEmpty()
  @IsNumberString()
  SERVER_PORT: number;

  @IsNotEmpty()
  @IsNumberString()
  DATABASE_PORT: number;

  @IsNotEmpty()
  @IsString()
  DATABASE_HOST: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_USER: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_PASSWORD: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_NAME: string;

  @IsNotEmpty()
  @IsString()
  COOKIE_SECRET_KEY: string;

  @IsString()
  @IsNotEmpty()
  ACCESS_TOKEN_SECRET: number;

  @IsNotEmpty()
  @IsString()
  REFRESH_TOKEN_SECRET: string;
}
