import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { CoursesCotroller } from './courses.cotroller';
import { CoursesService } from './courses.service';
import { Course } from './entities/courses.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Course])],
  controllers: [CoursesCotroller],
  providers: [CoursesService, JwtService],
})
export class CoursesModule {}
