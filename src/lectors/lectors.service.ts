import { DataSource, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import {
  CreateLectorRequestDto,
  CreateLectorResponseDto,
  FindAllLectorsResponseDto,
  LectorCoursesResponseDto,
  LectorStudentsResponseDto,
  UpdateLectoRequestDto,
  UpdateLectorResponseDto,
  addLectorCourseRequestDto,
} from './dto';
import { unifyStudents } from './utils';
import { QueryFilterDto } from '../application/dto';

Injectable({});
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorsRepository: Repository<Lector>,
    @InjectDataSource() private readonly dataSourse: DataSource,
  ) {}

  async findAll(query: QueryFilterDto): Promise<FindAllLectorsResponseDto> {
    const { offset, limit, order, sort, name } = query;
    const page = (offset - 1) * limit;

    const [lectors, count] = await this.lectorsRepository
      .createQueryBuilder('lectors')
      .select('lectors')
      .where('lectors.name ilike :name', { name: `%${name ? name : ''}%` })
      .orderBy(`lectors.${sort}`, `${order}`)
      .skip(page)
      .take(limit)
      .getManyAndCount();

    return { lectors, count };
  }

  async findOneCourses(id: number): Promise<LectorCoursesResponseDto | object> {
    const lector = await this.lectorsRepository
      .createQueryBuilder('lector')
      .select('lector')
      .leftJoin('lector.courses', 'courses')
      .addSelect(['courses.name', 'courses.description', 'courses.hours'])
      .where('lector.id = :id', { id })
      .getOne();

    if (!lector) return {};

    return lector;
  }

  async create(dto: CreateLectorRequestDto): Promise<CreateLectorResponseDto> {
    const hashedPassword = await bcrypt.hash(dto.password, 10);

    return await this.lectorsRepository.save({
      ...dto,
      password: hashedPassword,
    });
  }

  async addCourse(dto: addLectorCourseRequestDto): Promise<void> {
    await this.dataSourse
      .createQueryBuilder()
      .insert()
      .into('lector_course')
      .values({ lector_id: dto.lectorId, course_id: dto.courseId })
      .execute();
  }

  async findOneStudents(
    id: number,
  ): Promise<LectorStudentsResponseDto | object> {
    const lector = await this.lectorsRepository
      .createQueryBuilder('lectors')
      .select(['lectors', 'marks'])
      .leftJoin('lectors.marks', 'marks')
      .leftJoin('marks.student', 'student')
      .addSelect([
        'student.id',
        'student.name',
        'student.surname',
        'student.email',
        'student.age',
        'student.groupId',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name')
      .where('lectors.id = :id', { id })
      .getOne();

    if (!lector) return {};

    const newLectorObject = unifyStudents(lector);

    return newLectorObject;
  }

  async patch(
    id: number,
    dto: UpdateLectoRequestDto,
  ): Promise<UpdateLectorResponseDto> {
    const result = await this.lectorsRepository.update(id, dto);

    if (!result.affected) throw new NotFoundException('Lector not found');

    return await this.lectorsRepository.findOneBy({ id });
  }

  async delete(id: number): Promise<void> {
    const result = await this.lectorsRepository.delete(id);

    if (!result.affected) throw new NotFoundException('Lector not found');
  }
}
