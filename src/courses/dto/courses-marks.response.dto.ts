import { ApiProperty } from '@nestjs/swagger';

export class CoursesMarksResponseDto {
  @ApiProperty({ default: 'Math' })
  course_name: string;

  @ApiProperty({ default: 'Smith' })
  lector_name: string;

  @ApiProperty({ default: 'John' })
  student_name: string;

  @ApiProperty({ default: 70 })
  mark: number;
}
