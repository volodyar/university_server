import {
  IsEmail,
  IsString,
  IsNotEmpty,
  Length,
  Matches,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AuthRequestDto {
  @ApiProperty({ default: 'johnsmith@gmail.com' })
  @IsEmail()
  @IsNotEmpty()
  @Length(4)
  email: string;

  @ApiProperty({ default: '12345678' })
  @IsString()
  @IsNotEmpty()
  @Length(6, 16)
  @Matches(/^[^\s]+$/, { message: 'Spaces are not allowed in password field' })
  password: string;
}
