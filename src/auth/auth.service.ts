import 'dotenv/config';
import { IsNull, Not, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { ResetTokenService } from './reset-token.service';
import {
  AuthRequestDto,
  AuthResponseDto,
  RefreshTokenResponseDto,
  ResetPasswordRequestDto,
  ResetPasswordWithTokenRequestDto,
} from './dto';
import { Lector } from '../lectors/entities/lector.entity';
import { MailService } from '../mailer/mailer.service';

@Injectable({})
export class AuthService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorRepository: Repository<Lector>,
    private readonly jwtService: JwtService,
    private readonly resetTokenService: ResetTokenService,
    private readonly mailService: MailService,
  ) {}

  async signup(dto: AuthRequestDto): Promise<void> {
    await this.lectorRepository.save({
      ...dto,
      password: await this.hashData(dto.password),
    });
  }

  async signin(dto: AuthRequestDto): Promise<AuthResponseDto> {
    const user = await this.lectorRepository.findOneBy({ email: dto.email });

    if (!user) throw new ForbiddenException('Email or password invalid');

    const comparedPasswords = await bcrypt.compare(dto.password, user.password);

    if (!comparedPasswords)
      throw new ForbiddenException('Email or password invalid');

    const { access_token, refresh_token } = await this.getTokens(
      user.id,
      user.email,
    );

    await this.updateRefreshToken(user.id, refresh_token);

    return {
      email: user.email,
      name: user.name,
      refresh_token,
      access_token,
    };
  }

  async resetPasswordRequest({
    email,
  }: ResetPasswordRequestDto): Promise<void> {
    const user = await this.lectorRepository.findOneBy({ email });

    if (!user) throw new ForbiddenException('User is not found');

    const { token } = await this.resetTokenService.generateResetToken(user.id);

    await this.mailService.sendResetPassword(user, token);
  }

  async resetPassword(dto: ResetPasswordWithTokenRequestDto): Promise<void> {
    const { token, password } = dto;

    const resetPasswordRequest = await this.resetTokenService.findOne(token);

    if (!resetPasswordRequest) {
      throw new BadRequestException(
        'There no registered reset password operation by the token',
      );
    }

    const user = await this.lectorRepository.findOneBy({
      id: resetPasswordRequest.id,
    });

    if (!user) throw new ForbiddenException('User not found');

    const hashedPassword = await bcrypt.hash(password, 10);

    await this.lectorRepository.update(user.id, {
      password: hashedPassword,
    });
    await this.resetTokenService.delete(resetPasswordRequest.id);
  }

  async logout(id: number): Promise<void> {
    await this.lectorRepository.update(
      { id, refreshToken: Not(IsNull()) },
      { refreshToken: null },
    );
  }

  async refreshToken(id: number, rt: string): Promise<RefreshTokenResponseDto> {
    const user = await this.lectorRepository.findOneBy({ id });

    if (!user && !user.refreshToken)
      throw new ForbiddenException('Access Denied');

    const comparedTokens = user.refreshToken === rt;

    if (!comparedTokens) throw new ForbiddenException('Access Denied');

    const { access_token } = await this.getTokens(user.id, user.email);

    return { access_token };
  }

  async updateRefreshToken(userId: number, rt: string): Promise<void> {
    await this.lectorRepository.update(userId, { refreshToken: rt });
  }

  async hashData(value: string): Promise<string> {
    return await bcrypt.hash(value, 10);
  }

  async getTokens(
    userId: number,
    email: string,
  ): Promise<Omit<AuthResponseDto, 'email'>> {
    const atSecret = process.env.ACCESS_TOKEN_SECRET;
    const rtSecret = process.env.REFRESH_TOKEN_SECRET;

    const [at, rt] = await Promise.all([
      this.jwtService.signAsync(
        {
          sub: userId,
          email,
        },
        { expiresIn: 60 * 15, secret: atSecret },
      ),
      this.jwtService.signAsync(
        {
          sub: userId,
          email,
        },
        {
          expiresIn: 60 * 60 * 24 * 7,
          secret: rtSecret,
        },
      ),
    ]);

    return { access_token: at, refresh_token: rt };
  }
}
