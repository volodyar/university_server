import {
  Controller,
  Get,
  Post,
  UseGuards,
  Body,
  Query,
  UseFilters,
  HttpStatus,
  HttpCode,
  Param,
  Delete,
  Patch,
  UsePipes,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiTags,
  ApiBadRequestResponse,
  ApiNoContentResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiCookieAuth,
  ApiUnauthorizedResponse,
  ApiNotFoundResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { LectorsService } from './lectors.service';
import { AuthGuard } from '../auth/guard';
import { QueryFailedErrorFilter } from '../application/exception';
import {
  CreateLectorRequestDto,
  LectorCoursesResponseDto,
  addLectorCourseRequestDto,
  LectorStudentsResponseDto,
  CreateLectorResponseDto,
  UpdateLectoRequestDto,
  FindAllLectorsResponseDto,
  UpdateLectorResponseDto,
} from './dto';
import { QueryFilterDto } from '../application/dto';
import { TrimPipe } from '../application/pipes';

@ApiTags('Lectors')
@ApiCookieAuth()
@UseGuards(AuthGuard)
@UseFilters(QueryFailedErrorFilter)
@Controller('api/v1/lectors')
export class LectorsController {
  constructor(private readonly lectorsService: LectorsService) {}

  @ApiOperation({ summary: 'Get an array of lectors' })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: FindAllLectorsResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/')
  findAll(@Query() query: QueryFilterDto): Promise<FindAllLectorsResponseDto> {
    return this.lectorsService.findAll(query);
  }

  @ApiOperation({ summary: 'Create the lector' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: CreateLectorResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Bad Request | Credentials taken | Validation Error',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @UsePipes(new TrimPipe())
  @Post('/')
  create(
    @Body() dto: CreateLectorRequestDto,
  ): Promise<CreateLectorResponseDto> {
    return this.lectorsService.create(dto);
  }

  @ApiOperation({ summary: 'Add course to the lector' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiBadRequestResponse({
    description: "Bad Request | Credentials taken | Credentials don't exist",
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Post('/add-course')
  @HttpCode(HttpStatus.NO_CONTENT)
  addCourse(@Body() dto: addLectorCourseRequestDto): Promise<void> {
    return this.lectorsService.addCourse(dto);
  }

  @ApiOperation({ summary: "Get the lector's courses" })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: LectorCoursesResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/courses')
  findOneCourses(
    @Query('lector') id: number,
  ): Promise<LectorStudentsResponseDto | object> {
    return this.lectorsService.findOneCourses(id);
  }

  @ApiOperation({ summary: "Get the lector's students" })
  @ApiOkResponse({
    description: 'The record has been successfully retrieved.',
    type: LectorStudentsResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('/students')
  findOneStudents(
    @Query('lector') id: number,
  ): Promise<LectorStudentsResponseDto | object> {
    return this.lectorsService.findOneStudents(id);
  }

  @ApiOperation({ summary: "Update the lector's data" })
  @ApiOkResponse({ type: UpdateLectorResponseDto })
  @ApiNotFoundResponse({ description: 'Not Found | Lector not found' })
  @ApiBadRequestResponse({
    description: "Bad Request | Credentials don't exist | Validation error",
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  async patch(
    @Param('id') id: number,
    @Body() dto: UpdateLectoRequestDto,
  ): Promise<UpdateLectorResponseDto> {
    return await this.lectorsService.patch(id, dto);
  }

  @ApiOperation({ summary: 'Delete the lector' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Not Found | Lector not found' })
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id') id: number): Promise<void> {
    return await this.lectorsService.delete(id);
  }
}
