import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordResponseDto {
  @ApiProperty({ default: 1 })
  id: number;

  @ApiProperty({
    default: '52a6633cd6608583b4ac3f7865a5ba6fe457078cdef4c64d326375a0222197b5',
  })
  token: string;
}
