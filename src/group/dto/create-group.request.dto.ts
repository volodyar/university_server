import { IsNotEmpty, IsString, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateGroupRequestDto {
  @ApiProperty({ default: 'MS-40' })
  @IsString()
  @IsNotEmpty()
  @Length(2, 64)
  name: string;
}
