# Development stage
FROM node:18 as development

ENV NODE_ENV=${NODE_ENV}

WORKDIR /app

COPY package*.json .

RUN npm ci
RUN npm install

COPY . .

CMD npm run start:dev


# Production stage
FROM node:18-alpine as base

WORKDIR /app

FROM base as build

COPY package*.json .

RUN npm ci

COPY . .

RUN npm run build
RUN npm i --omit=dev

FROM base as production

ENV NODE_ENV=${NODE_ENV}

COPY --from=build app/node_modules ./node_modules
COPY --from=build app/dist ./dist

COPY package*.json .

CMD npm start