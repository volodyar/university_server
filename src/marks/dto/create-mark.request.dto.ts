import { IsNumber, Max, Min } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class CreateMarkRequestDto {
  @ApiProperty({ default: 75 })
  @IsNumber()
  @Type(() => Number)
  @Min(1)
  @Max(100)
  mark: number;

  @ApiProperty({ default: 1 })
  @IsNumber()
  @Type(() => Number)
  @Min(1)
  courseId: number;

  @ApiProperty({ default: 1 })
  @IsNumber()
  @Type(() => Number)
  @Min(1)
  lectorId: number;

  @ApiProperty({ default: 1 })
  @IsNumber()
  @Type(() => Number)
  @Min(1)
  studentId: number;
}
