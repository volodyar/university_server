import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ResetTokenService } from './reset-token.service';
import { ResetToken } from './entities/reset-token.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { MailModule } from '../mailer/mailer.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ResetToken, Lector]),
    JwtModule.register({}),
    MailModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, ResetTokenService],
})
export class AuthModule {}
