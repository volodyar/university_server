import { CreateStudentResponseDto } from './create-student.response.dto';

export class UpdateStudentResponseDto extends CreateStudentResponseDto {}
