import { Entity, Column, OneToMany } from 'typeorm';
import { CommonEntity } from '../../application/entity';
import { Student } from '../../students/entities/students.entity';

@Entity({ name: 'groups' })
export class Group extends CommonEntity {
  @Column({ type: 'varchar', unique: true })
  name: string;

  @OneToMany(() => Student, (student) => student.group, {
    onDelete: 'SET NULL',
  })
  students: Student[];
}
