import { ApiProperty } from '@nestjs/swagger';
import { CreateCourseRequestDto } from './create-course.request.dto';

export class CreateCourseResponseDto extends CreateCourseRequestDto {
  @ApiProperty({ default: 1 })
  id: number;

  @ApiProperty({ default: '2023-08-25T06:48:20.904Z' })
  createdAt: Date;

  @ApiProperty({ default: '2023-08-25T06:48:20.904Z' })
  updatedAt: Date;

  @ApiProperty({ default: '1' })
  studentsCount?: number;
}
