import { Entity, Column, ManyToMany, JoinTable, OneToMany } from 'typeorm';
import { CommonEntity } from '../../application/entity';
import { Course } from '../../courses/entities/courses.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'lectors' })
export class Lector extends CommonEntity {
  @Column({ type: 'varchar', nullable: true })
  name: string;

  @Column({ type: 'varchar', nullable: true })
  surname: string;

  @Column({ type: 'varchar', unique: true })
  email: string;

  @Column({ type: 'varchar' })
  password: string;

  @Column({ type: 'varchar', nullable: true, name: 'refresh_token' })
  refreshToken: string;

  @ManyToMany(() => Course, (course) => course.lectors, {
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
    nullable: true,
  })
  @JoinTable({
    name: 'lector_course',
    joinColumn: {
      name: 'lector_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector, {
    onDelete: 'SET NULL',
  })
  marks: Mark[];
}
