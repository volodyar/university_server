import { ApiProperty } from '@nestjs/swagger';
import { StudentCoursesResponseDto } from './student-courses.response.dto';

export class FindAllStudentsResponseDto {
  @ApiProperty({
    type: [StudentCoursesResponseDto],
  })
  students: StudentCoursesResponseDto[];

  @ApiProperty({ default: 1 })
  count: number;
}
