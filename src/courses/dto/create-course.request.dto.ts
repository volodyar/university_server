import { IsNotEmpty, IsNumber, IsString, Length, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class CreateCourseRequestDto {
  @ApiProperty({ default: 'Math' })
  @IsString()
  @IsNotEmpty()
  @Length(2, 64)
  name: string;

  @ApiProperty({ default: 'A lot useful information' })
  @IsString()
  @IsNotEmpty()
  @Length(4, 240)
  description: string;

  @ApiProperty({ default: 24 })
  @IsNumber()
  @Min(2)
  @Type(() => Number)
  hours: number;
}
