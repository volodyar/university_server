export { CreateLectorRequestDto } from './create-lector.request.dto';
export { LectorCoursesResponseDto } from './lector-courses.response.dto';
export { LectorStudentsResponseDto } from './lector-students.response.dto';
export { CreateLectorResponseDto } from './create-lector.response.dto';
export { addLectorCourseRequestDto } from './add-lector-course.request.dto';
export { UpdateLectoRequestDto } from './update-lector.request.dto';
export { FindAllLectorsResponseDto } from './find-all-lectors.response';
export { UpdateLectorResponseDto } from './update-lector.response.dto';
