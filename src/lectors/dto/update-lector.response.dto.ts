import { CreateLectorResponseDto } from './create-lector.response.dto';

export class UpdateLectorResponseDto extends CreateLectorResponseDto {}
