import { ApiProperty } from '@nestjs/swagger';
import { CreateMarkRequestDto } from './create-mark.request.dto';

export class CreateMarkResponseDto extends CreateMarkRequestDto {
  @ApiProperty({ default: 1 })
  id: number;

  @ApiProperty({ default: '2023-08-25T06:48:20.904Z' })
  createdAt: Date;

  @ApiProperty({ default: '2023-08-25T06:48:20.904Z' })
  updatedAt: Date;
}
