import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddUserIdToResetTokenTable1693729746154
  implements MigrationInterface
{
  name = 'AddUserIdToResetTokenTable1693729746154';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "reset_token" ADD "userId" integer NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "reset_token" DROP COLUMN "userId"`);
  }
}
