import { Repository } from 'typeorm';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { randomBytes } from 'crypto';
import { ResetToken } from './entities/reset-token.entity';
import { ResetPasswordResponseDto } from './dto';

@Injectable({})
export class ResetTokenService {
  constructor(
    @InjectRepository(ResetToken)
    private readonly resetTokenRepository: Repository<ResetToken>,
  ) {}

  async generateResetToken(
    id: number,
  ): Promise<Pick<ResetPasswordResponseDto, 'token'>> {
    const token = randomBytes(32).toString('hex');

    const response = await this.resetTokenRepository.save({
      userId: id,
      token,
    });

    return { token: response.token };
  }

  async findOne(token: string): Promise<ResetPasswordResponseDto> {
    return await this.resetTokenRepository.findOneBy({ token });
  }

  async delete(id: number): Promise<void> {
    const res = await this.resetTokenRepository.delete(id);

    if (!res.affected) throw new BadRequestException('Token is not found');
  }
}
