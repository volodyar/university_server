import { StudentWithGroupDto } from '../../students/dto';
import { LectorStudentsResponseDto } from '../dto';
import { Lector } from '../entities/lector.entity';

export function unifyStudents(lector: Lector): LectorStudentsResponseDto {
  const students = lector.marks.map((mark) => mark.student);
  const uniqStudents: StudentWithGroupDto[] = [
    ...new Map(students.map((student) => [student.email, student])).values(),
  ];

  return {
    id: lector.id,
    name: lector.name,
    surname: lector.surname,
    email: lector.email,
    password: lector.password,
    createdAt: lector.createdAt,
    updatedAt: lector.updatedAt,
    students: uniqStudents,
  };
}
