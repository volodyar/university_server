import {
  IsEmail,
  IsString,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Min,
  Max,
  Length,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
export class CreateStudentRequestDto {
  @IsString()
  @IsNotEmpty()
  @Length(2, 64)
  @ApiProperty({
    default: 'John',
  })
  name: string;

  @IsString()
  @IsNotEmpty()
  @Length(2, 64)
  @ApiProperty({
    default: 'Smith',
  })
  surname: string;

  @IsEmail()
  @IsString()
  @IsNotEmpty()
  @Length(2, 64)
  @ApiProperty({
    default: 'johnsmith@gmail.com',
  })
  email: string;

  @IsNumber()
  @Min(15)
  @Max(60)
  @ApiProperty({
    default: 19,
  })
  @Type(() => Number)
  age: number;

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    default: null,
  })
  imagePath?: string | null;

  @ApiProperty({
    default: 1,
  })
  @IsNumber()
  @IsOptional()
  @Min(1)
  @Type(() => Number)
  groupId?: number | null;
}
