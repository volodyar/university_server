import { Module } from '@nestjs/common';
import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';
import { StudentModule } from '../students/students.module';
import { EnvConfigModule } from '../configs/config.module';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from '../database/database.module';
import { GroupsModule } from '../group/groups.module';
import { AuthModule } from '../auth/auth.module';
import { LectorsModule } from '../lectors/lectors.module';
import { CoursesModule } from '../courses/courses.module';
import { MarksModule } from '../marks/marks.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    EnvConfigModule,
    DatabaseModule,
    AuthModule,
    StudentModule,
    GroupsModule,
    LectorsModule,
    CoursesModule,
    MarksModule,
  ],
})
export class AppModule {}
