import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import { CommonEntity } from '../../application/entity';
import { Student } from '../../students/entities/students.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Course } from '../../courses/entities/courses.entity';

@Entity({ name: 'marks' })
export class Mark extends CommonEntity {
  @Column({ type: 'integer' })
  mark: number;

  @Column({ name: 'course_id', type: 'integer' })
  courseId: number;

  @Column({ name: 'student_id', type: 'integer' })
  studentId: number;

  @Column({ name: 'lector_id', type: 'integer' })
  lectorId: number;

  @ManyToOne(() => Course, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ManyToOne(() => Student, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @ManyToOne(() => Lector, {
    nullable: false,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;
}
