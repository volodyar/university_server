import 'dotenv/config';
import { extname } from 'path';
import { DataSourceOptions } from 'typeorm';

export const databaseConfiguration = (
  isMigrationRun = true,
): DataSourceOptions => {
  const migrationFolder = __dirname + '/../../database/migrations';
  const ext = extname(__filename);

  return {
    type: 'postgres',
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    entities: [__dirname + `/../../**/*.entity${ext}`],
    migrations: [`${migrationFolder}/*${ext}`],
    migrationsTableName: 'migrations',
    migrationsRun: isMigrationRun,
    logging: true,
    synchronize: false,
    ssl: true,
  };
};
