import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddLectorSurnameField1693913298545 implements MigrationInterface {
  name = 'AddLectorSurnameField1693913298545';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectors" ADD "surname" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lectors" DROP COLUMN "surname"`);
  }
}
