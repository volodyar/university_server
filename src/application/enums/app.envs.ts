export enum AppEnvs {
  'LOCAL' = 'local',
  'DEVELOP' = 'development',
  'PRODUCTION' = 'production',
  'TEST' = 'test',
}
